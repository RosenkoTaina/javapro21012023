package com.hillel.rosenko.repository;

import com.hillel.rosenko.Order;
import com.hillel.rosenko.Product;

import java.util.Collection;
import java.util.UUID;

public class OrderRepository extends RepositoryBase {

    public Order addOrder(Order order) {
        order.setId(UUID.randomUUID());
        order.getProducts().setId(UUID.randomUUID());
        storage.put(order.getId(), order);
        return order;
    }

    public Order getOrder(UUID id) {
        return storage.get(id);
    }

    public Collection<Order> getOrders() {
        return storage.values();
    }

    public Order updateOrder(Order order) {
        Order existingOrder = storage.get(order.getId());
        existingOrder.setDate(order.getDate());
        existingOrder.setCost(order.getCost());
        Product existingProduct = existingOrder.getProducts();
        Product updatedProduct = order.getProducts();
        existingProduct.setName(updatedProduct.getName());
        existingProduct.setCost(updatedProduct.getCost());

        return existingOrder;
    }

    public void deleteOrder(UUID id) {
        storage.remove(id);
    }


}

