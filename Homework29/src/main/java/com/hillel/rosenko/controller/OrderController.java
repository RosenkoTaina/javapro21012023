package com.hillel.rosenko.controller;


import com.hillel.rosenko.Order;
import com.hillel.rosenko.service.OrderService;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path("/orders")
public class OrderController {

    OrderService service = new OrderService();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addOrder(Order order) {

        return Response.ok(service.addOrder(order)).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrder(@PathParam("id") UUID id) {
        return Response.ok(service.getOrder(id)).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrders() {
        return Response.ok(service.getOrders()).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateOrder(Order order) {
        return Response.ok(service.updateOrder(order)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteOrder(@PathParam("id") UUID id) {
        String message = "Product with ID - " + id + " successfully deleted";
        service.deleteOrder(id);
        return Response.ok(message).build();
    }

}
