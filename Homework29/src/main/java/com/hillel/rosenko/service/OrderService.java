package com.hillel.rosenko.service;

import com.hillel.rosenko.Order;
import com.hillel.rosenko.repository.OrderRepository;

import java.util.Collection;
import java.util.UUID;


public class OrderService {

    OrderRepository repo = new OrderRepository();

    public Order addOrder(Order order) {
        return repo.addOrder(order);
    }

    public Order getOrder(UUID id) {
        return repo.getOrder(id);
    }

    public Collection<Order> getOrders() {
        return repo.getOrders();
    }

    public Order updateOrder(Order order) {
        return repo.updateOrder(order);
    }

    public void deleteOrder(UUID id) {
        repo.deleteOrder(id);
    }

}
