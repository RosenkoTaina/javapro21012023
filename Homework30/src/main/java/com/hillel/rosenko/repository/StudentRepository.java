package com.hillel.rosenko.repository;

import com.hillel.rosenko.entity.Student;

public class StudentRepository extends CrudRepository<Student> {

    protected StudentRepository() {
        super(Student.class);
    }
}

