package com.hillel.rosenko.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;
import org.hibernate.query.criteria.JpaCriteriaQuery;
import com.hillel.rosenko.enums.HibernateSession;


public abstract class CrudRepository<T> {

    private final Class<T> entityClass;

    protected CrudRepository(Class<T> entityClass) {
        this.entityClass = entityClass;
    }


    public void add(T entity) {
        try (Session session = HibernateSession.INSTANCE.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(entity);
            transaction.commit();
        }
    }

    public T getById(Long id) {
        try (Session session = HibernateSession.INSTANCE.openSession()) {
            T entity = session.get(entityClass, id);
            System.out.println(entity);
            if (entity == null) {
                System.out.println("Entity with ID " + id + " not found.");
            }
            return entity;
        } catch (Exception e) {
            System.out.println("Error retrieving entity by ID: " + e.getMessage());
            return null;
        }
    }

    public List<T> getAll() {
        try (Session session = HibernateSession.INSTANCE.openSession()) {

            HibernateCriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            JpaCriteriaQuery<T> query = criteriaBuilder.createQuery(entityClass);
            query.from(entityClass);

            List<T> resultList = session.createQuery(query).getResultList();

            System.out.println("Retrieved " + resultList.size() + " entities from the database:");

            for (T entity : resultList) {
                System.out.println(entity);
            }

            return resultList;
        }
    }


    public void update(T entity) {
        try (Session session = HibernateSession.INSTANCE.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(entity);
            transaction.commit();
        }
    }


    public void delete(Long id) {
        try (Session session = HibernateSession.INSTANCE.openSession()) {
            Transaction transaction = session.beginTransaction();

            T entityToDelete = session.get(entityClass, id);
            if (entityToDelete != null) {
                session.delete(entityToDelete);
                transaction.commit();
                System.out.println("Record with ID " + id + " deleted successfully.");
            } else {
                System.out.println("Record with ID " + id + " does not exist.");
            }
        } catch (Exception e) {
            System.out.println("Error deleting record with ID " + id + ": " + e.getMessage());
        }
    }

}

