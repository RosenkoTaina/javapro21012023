package com.hillel.rosenko.enums;

import com.hillel.rosenko.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public enum HibernateSession {

    INSTANCE;

    private final SessionFactory sessionFactory;

    HibernateSession() {

        sessionFactory = new Configuration().configure("HibernateSessionConfiguration.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();
    }

    public Session openSession() {
        return sessionFactory.openSession();
    }

}


