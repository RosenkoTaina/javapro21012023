package com.hillel.rosenko.repository;

import java.util.List;

import com.hillel.rosenko.entity.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StudentRepositoryTest {

    CrudRepository<Student> repo = new StudentRepository();

    @Test
    void addStudentTest() {

        Student student = Student.builder()
                .name("Student 4")
                .email("nstudent@zunu.com")
                .build();

        repo.add(student);

    }

    @Test
    void getByIdTest() {

        Student student = repo.getById(4L);
            Assertions.assertEquals("Student 3", student.getName());

    }

    @Test
    void getAll() {

        List<Student> all = repo.getAll();

        Assertions.assertTrue(all.size() < 10);
    }

    @Test
    void updateStudent() {

        Student student = repo.getById(5L);
        student.setName("new name F");
        student.setEmail("emai2@qsad.com");

        repo.update(student);
        repo.getById(4L);
    }

    @Test
    void deleteStudent() {

        repo.delete(3L);
    }

}
