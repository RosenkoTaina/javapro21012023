package com.hillel.rosenko.repository;

import com.hillel.rosenko.entity.Order;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

public class OrderRepository {
    HashMap<UUID, Order> storage = new HashMap<>();

    public void add(Order order) {
        storage.put(order.getId(), order);
    }

    public Order getById(UUID uuid) {
        return storage.get(uuid);
    }

    public Collection<Order> getAll() {
        return storage.values();
    }
}
