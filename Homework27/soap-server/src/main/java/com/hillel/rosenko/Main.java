package com.hillel.rosenko;

import com.hillel.rosenko.service.OrderService;
import lombok.SneakyThrows;

import javax.xml.ws.Endpoint;

public class Main {
    private static final String ADDRESS = "http://localhost:16052/Orders?wsdl";
    private static final Object SERVICE = new OrderService();

    public static void main(String[] args) {

        Endpoint endpoint = Endpoint.publish(ADDRESS, SERVICE);


        System.out.println("Endpoint: " + ADDRESS);

        sleep();
        endpoint.stop();

    }

    @SneakyThrows
    private static void sleep() {

        while (true)
            Thread.sleep(100000);
    }
}
