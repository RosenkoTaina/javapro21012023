package com.hillel.rosenko.service;

import com.hillel.rosenko.entity.Order;
import com.hillel.rosenko.repository.OrderRepository;

import javax.jws.WebService;
import java.util.Collection;
import java.util.UUID;

@WebService
public class OrderService {

    OrderRepository orderRepository = new OrderRepository();

    public void add(Order order) {

        orderRepository.add(order);
    }

    public Order getById(UUID uuid) {

        return orderRepository.getById(uuid);
    }

    public Collection<Order> getAll() {

        return orderRepository.getAll();
    }


}
