package com.hillel.rosenko;

import com.hillel.rosenko.client.generated.Order;
import com.hillel.rosenko.client.generated.OrderService;
import com.hillel.rosenko.client.generated.OrderServiceService;
import com.hillel.rosenko.client.generated.Product;

import java.util.UUID;

public class Main {
    public static void main(String[] args) {

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");

        OrderService service = new OrderServiceService().getOrderServicePort();
        Product product = new Product();
        product.setId(UUID.randomUUID().toString());
        product.setCost(13.37);
        product.setName("Test Code Product");

        Order order = new Order();
        order.setId(UUID.randomUUID().toString());
        order.setDate();
        order.setCost(60.04);
        order.setProducts(product);

        service.add(order);
        service.getById("91654634-12eb-11ee-be56-0242ac120002");
        service.getAll();

    }
}
