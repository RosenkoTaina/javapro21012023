package com.hillel.rosenko.lessons.lesson18;

import java.util.*;


public class FileNavigator {
    private TreeMap<String, List<FileData>> fileMap;

    public FileNavigator() {
        fileMap = new TreeMap<>();
    }

    public void add(FileData fileData) {
        String filePath = fileData.getFilePath();
        String pathKey = getPathKey(filePath);

        File file = new File(filePath);
        if (!file.exists() || !file.isFile() ) {
            System.out.println("This path doesnt exist: " + filePath);
            return;
        }

        if (!fileMap.containsKey(pathKey)) {
            fileMap.put(pathKey, new ArrayList<>());
        }

        List<FileData> files = fileMap.get(pathKey);
        files.add(fileData);
    }

    public List<FileData> find(String path) {
        String pathKey = getPathKey(path);
        return fileMap.getOrDefault(pathKey, new ArrayList<>());
    }

    public List<FileData> filterBySize(long maxSize) {
        List<FileData> filteredFiles = new ArrayList<>();

        for (List<FileData> files : fileMap.values()) {
            for (FileData file : files) {
                if (file.getFileSize() <= maxSize) {
                    filteredFiles.add(file);
                }
            }
        }

        return filteredFiles;
    }

    public void remove(String path) {
        String pathKey = getPathKey(path);
        fileMap.remove(pathKey);
    }

    public List<FileData> sortBySize() {
        List<FileData> allFiles = new ArrayList<>();

        for (List<FileData> files : fileMap.values()) {
            allFiles.addAll(files);
        }

        allFiles.sort(Comparator.comparingLong(FileData::getFileSize));

        return allFiles;
    }

    private String getPathKey(String path) {
        return path.toLowerCase();
    }


    public void printFileList(String path, String operationType) {
        List<FileData> fileList;
        switch (operationType) {
            case "find":
                fileList = find(path);
                System.out.println("\nFiles in " + path + ":");
                break;
            case "filter":
                long maxSize = Long.parseLong(path);
                fileList = filterBySize(maxSize);
                System.out.println("Files with size <= " + maxSize + " bytes:");
                break;
            case "sort":
                fileList = sortBySize();
                System.out.println("Sorted files by size:");
                break;
            default:
                System.out.println("Invalid operation type.");
                return;
        }
        for (FileData fileData : fileList) {
            System.out.println("File Name: " + fileData.getFileName());
            System.out.println("File Size: " + fileData.getFileSize() + " bytes");
            System.out.println();
        }
    }

    public void printFiles() {
        for (String path : fileMap.keySet()) {
            System.out.println("Path: " + path);
            List<FileData> files = fileMap.get(path);
            for (FileData file : files) {
                System.out.println(" - " + file);
            }
            System.out.println();
        }
    }

}
