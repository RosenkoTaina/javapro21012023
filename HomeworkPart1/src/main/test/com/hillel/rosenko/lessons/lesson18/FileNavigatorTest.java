package com.hillel.rosenko.lessons.lesson18;

import java.util.*;
class FileNavigatorTest {
    public static void main(String[] args) {

        FileNavigator fileNavigator = new FileNavigator();


        FileData file1 = new FileData("test1.txt", 3072, "/Rosenko_JavaPro_31012023/PakageToTest1");
        FileData file2 = new FileData("test2.txt", 31744, "/Rosenko_JavaPro_31012023/PakageToTest1");
        FileData file3 = new FileData("test3.txt", 1024, "/Rosenko_JavaPro_31012023/PakageToTest2");
        FileData file4 = new FileData("test4.txt", 0, "/Rosenko_JavaPro_31012023/PakageToTest2");
        fileNavigator.add(file1);
        fileNavigator.add(file2);
        fileNavigator.add(file3);
        fileNavigator.add(file4);


        List<FileData> filesAtPath = fileNavigator.find("/Rosenko_JavaPro_31012023/PakageToTest1");
        System.out.println("Files at /path/to/files:");
        for (FileData file : filesAtPath) {
            System.out.println(file.getFileName());
        }


        long maxSize = 3000;
        List<FileData> filteredFiles = fileNavigator.filterBySize(maxSize);
        System.out.println("Files with size <= " + maxSize + " bytes:");
        for (FileData file : filteredFiles) {
            System.out.println(file.getFileName());
        }




        List<FileData> sortedFiles = fileNavigator.sortBySize();
        System.out.println("Files sorted by size:");
        for (FileData file : sortedFiles) {
            System.out.println(file.getFileName() + " - " + file.getFileSize() + " bytes");
        }



        String pathToRemove = "/Rosenko_JavaPro_31012023/PakageToTest2";
        fileNavigator.remove(pathToRemove);
        System.out.println("Files after removing " + pathToRemove + ":");
        filesAtPath = fileNavigator.find("/path/to/files");
        for (FileData file : filesAtPath) {
            System.out.println(file.getFileName());
        }


    }
}

